﻿using System;
using System.Collections.Generic;
using System.Linq;
using Google.Protobuf.Collections;
using Otus.Teaching.PromoCodeFactory.WebHost.gRPC;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class gRpcCustomerMapper
    {
        public static Guid GetCustomerId(CustomerId customerId)
        {
            Guid result = ParseStringGuid(customerId.Id);

            return result;
        }

        public static Guid ParseStringGuid(string id)
        {
            Guid result = Guid.Empty;

            Guid.TryParse(id, out result);

            return result;
        }

        public static List<CustomerShortResponse> MapCustomerShortResponses(
                ListCustomerShortGrpcResponse listCustomerShortResponse)
        {
            var responses = listCustomerShortResponse.CustomerShortGrpcResponses.Select(c =>
            {
                return MapCustomerShortResponseFromGrpc(c);
            }).ToList();

            return responses;
        }

        public static ListCustomerShortGrpcResponse MapListCustomerShortGrpcResponse(
                List<CustomerShortResponse> responses)
        {

            var customerResponses = new List<CustomerShortGrpcResponse>();

            foreach (var customerShortResponse in responses)
            {
                customerResponses.Add(MapCustomerShortGrpcResponse(customerShortResponse));
            }

            ListCustomerShortGrpcResponse grpcResponse = new ListCustomerShortGrpcResponse()
            {
                CustomerShortGrpcResponses = { customerResponses }
            };

            return grpcResponse;
        }


        public static CustomerShortGrpcResponse MapCustomerShortGrpcResponse(
                CustomerShortResponse customerShortResponse)
        {
            var resp = new CustomerShortGrpcResponse()
            {
                    Id = customerShortResponse.Id.ToString(),
                    Email = customerShortResponse.Email,
                    FirstName = customerShortResponse.FirstName,
                    LastName = customerShortResponse.LastName
            };

            return resp;
        }

        public static CreateOrEditCustomerRequest MapCreateOrEditCustomerRequestFromGrpc(
                CreateOrEditCustomerGrpcRequest createOrEditCustomerGrpcRequest)
        {
            CreateOrEditCustomerRequest request = new CreateOrEditCustomerRequest
            {
                Email = createOrEditCustomerGrpcRequest.Email,
                FirstName = createOrEditCustomerGrpcRequest.FirstName,
                LastName = createOrEditCustomerGrpcRequest.LastName,
                PreferenceIds = createOrEditCustomerGrpcRequest.PreferencesIds.Select(c =>
                {
                    return ParseStringGuid(c);
                }).ToList()
            };

            return request;
        }

        

        public static CustomerShortResponse MapCustomerShortResponseFromGrpc(CustomerShortGrpcResponse grpcResponse)
        {
            CustomerShortResponse response = new CustomerShortResponse()
            {
                Id = ParseStringGuid(grpcResponse.Id),
                Email = grpcResponse.Email,
                FirstName = grpcResponse.FirstName,
                LastName = grpcResponse.LastName
            };

            return response;
        }


        public static CustomerGrpcResponse MapCustomerGrpcResponse(CustomerResponse response)
        {
            var preferences = new List<PreferenceGrpcResponse>();
            var promocodes = new List<PromoCodeShortGrpcResponse>();

            foreach (var preference in response.Preferences)      
            {
                preferences.Add(MapPreferenceGrpcResponse(preference));
            }

            foreach (var promoCodeShortResponse in response.PromoCodes)
            {
                promocodes.Add(MapPromoCodeShortGrpcResponse(promoCodeShortResponse));
            }

            var result = new CustomerGrpcResponse()
            {
                    Id = response.Id.ToString(),
                    LastName = response.LastName,
                    FirstName = response.FirstName,
                    Email = response.Email,
                    Preferences = { preferences },
                    PromoCodes = { promocodes }
            };

            return result;
        }

        public static PreferenceGrpcResponse MapPreferenceGrpcResponse(PreferenceResponse response)
        {
            var preference = new PreferenceGrpcResponse()
            {
                    Id = response.Id.ToString(),
                    Name = response.Name
            };

            return preference;
        }

        public static PromoCodeShortGrpcResponse MapPromoCodeShortGrpcResponse(PromoCodeShortResponse response)
        {
            var promocode = new PromoCodeShortGrpcResponse
            {
                    Id = response.Id.ToString(),
                    BeginDate = response.BeginDate,
                    EndDate = response.EndDate,
                    Code = response.Code,
                    PartnerName = response.PartnerName,
                    ServiceInfo = response.ServiceInfo
            };

            return promocode;
        }

        public static CreateOrEditCustomerRequest MapCreateOrEditCustomerRequest(
                CreateOrEditCustomerGrpcRequest grpcRequest)
        {
            var request = new CreateOrEditCustomerRequest()
            {
                    LastName = grpcRequest.LastName,
                    Email = grpcRequest.Email,
                    FirstName = grpcRequest.FirstName,
                    PreferenceIds = grpcRequest.PreferencesIds.Select(c =>
                    {
                        return ParseStringGuid(c);
                    }).ToList()
            };

            return request;
        }

        public static Tuple<Guid, CreateOrEditCustomerRequest> MapCreateOrEditCustomerRequestWithId(
                CreateOrEditCustomerGrpcRequestWithId requestWithId)
        {
            var result = Tuple.Create(GetCustomerId(requestWithId.Id), MapCreateOrEditCustomerRequest(requestWithId.CreateOrEditCustomerGrpcRequest));

            return result;
        }
    }
}