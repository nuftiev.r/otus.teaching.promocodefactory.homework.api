﻿
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Services.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.WebHost.gRPC
{
    public class GrpcCustomerService : GrpcCustomers.GrpcCustomersBase
    {
        private readonly ICustomerService _customerService;
        public GrpcCustomerService( ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public override async Task<ListCustomerShortGrpcResponse> GetCustomersAsync(Empty request, ServerCallContext context)
        {
            var customers = await _customerService.GetCustomersAsync();

            var result = gRpcCustomerMapper.MapListCustomerShortGrpcResponse(customers);

            return result;
        }

        public override async Task<CustomerGrpcResponse> GetCustomerAsync(CustomerId request, ServerCallContext context)
        {
            var id = gRpcCustomerMapper.GetCustomerId(request);

            var customer =  await _customerService.GetCustomerAsync(id);

           var result = gRpcCustomerMapper.MapCustomerGrpcResponse(customer);

           return result;
        }

        public override async Task<CustomerGrpcResponse> CreateCustomerAsync(CreateOrEditCustomerGrpcRequest request, ServerCallContext context)
        {
            var createRequest = gRpcCustomerMapper.MapCreateOrEditCustomerRequest(request);

            var customerResponse = await _customerService.CreateCustomerAsync(createRequest);

            var result = gRpcCustomerMapper.MapCustomerGrpcResponse(customerResponse);

            return result;
        }

        public override async Task<BoolValue> EditCustomersAsync(CreateOrEditCustomerGrpcRequestWithId request, ServerCallContext context)
        {
            var parserdRequest = gRpcCustomerMapper.MapCreateOrEditCustomerRequestWithId(request);

            var isEdit = await _customerService.EditCustomersAsync(parserdRequest.Item1, parserdRequest.Item2);

            BoolValue result = new BoolValue();

            result.Value = isEdit;

            return result;
        }

        public override async Task<BoolValue> DeleteCustomerAsync(CustomerId request, ServerCallContext context)
        {
            var id = gRpcCustomerMapper.GetCustomerId(request);

            var isDeleted = await _customerService.DeleteCustomerAsync(id);

            BoolValue result = new BoolValue();

            result.Value = isDeleted;

            return result;
        }
    }
}